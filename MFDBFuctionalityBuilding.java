
import java.sql.*;
import java.io.File;
import java.util.ArrayList;


public class MFDBFuctionalityBuilding {

    public static void main(String[] args) {
    	
        String dbURl = "jdbc:mysql://localhost/Movies";
        String user = "root";
        String pass = "amal";
        try{
        Connection connect = DriverManager.getConnection(dbURl,user,pass);
        Statement statement = connect.createStatement();
        System.out.println("Connection established");

        String query = "SELECT * FROM Files";
        
        File dir  = new File("/Volumes/A");
        ArrayList<file> A = new ArrayList();

        find_Data scanner = new find_Data();
        scanner.find_Fdata(dir,A);

        insert(A, statement);
        display(query, statement);

        connect.close();

        }catch(SQLException e){
        	System.out.println("No Database created");
        	System.out.println(e.getMessage());
        }

    }public static void display(String query, Statement statement){
        try{
            ResultSet results = statement.executeQuery(query);
            System.out.println("File name\tfileType\tfileLoc\t\tlanguage");
            while(results.next()){
                String fileName = results.getString("fileName");
                String fileType = results.getString("fileType");
                String fileLoc = results.getString("fileLoc");
                String language = results.getString("language");
                System.out.println(fileName+"\t \t"+fileType+"\t \t"+fileLoc+"\t"+language);
            }
        }catch(SQLException ex){
            System.out.println("SQL Error:"+ex.getMessage());
        }
    }
    public static void insert(ArrayList<file> A, Statement statement){
        try{
            for(int i =0; i<A.size(); i++){
                String fileName = A.get(i).getFile_Name();
                String fileType = A.get(i).getFile_Type();
                String fileLoc = A.get(i).getFile_Loc();
                String language = A.get(i).getLanguage();
                String insert = "INSERT IGNORE INTO Files VALUES("+"\'"+fileName+"\'"+","+"\'"+fileType+"\'"+","+"\'"+fileLoc+"\'"+","+"\'"+language+"\'"+")";
                //System.out.println(insert);
                statement.executeUpdate(insert);
            }
            
        }catch(SQLException e){
            System.out.println("Error adding to Database");
            System.out.println(e.getMessage());
        }
        

    }


}
