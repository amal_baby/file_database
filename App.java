import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;


public class App extends Application{
	private final TableView<table_data> table = new TableView<table_data>();
	private ArrayList<file> filedata = new ArrayList();
	private Statement statement;
	private find_Data scanner = new find_Data();    
    private String query = "SELECT * FROM Files";
    private ObservableList<table_data> data = FXCollections.observableArrayList();
    private Connection connect;
	@Override
	public void start(Stage primaryStage){
		

		GridPane window = new GridPane();

		table.setEditable(true);

		TableColumn filename = new TableColumn("Movie Name");
		filename.setCellValueFactory(new PropertyValueFactory<table_data,String>("name"));
		filename.setMinWidth(150);

		TableColumn filetype = new TableColumn("File Format");
		filetype.setCellValueFactory(new PropertyValueFactory<table_data,String>("type"));
		filetype.setMinWidth(150);

		TableColumn fileloc = new TableColumn("File Location");
		fileloc.setCellValueFactory(new PropertyValueFactory<table_data,String>("loc"));
		fileloc.setMinWidth(150);

		TableColumn lang = new TableColumn("Languge");
		lang.setCellValueFactory(new PropertyValueFactory<table_data,String>("lang"));
		lang.setMinWidth(150);

		table.getColumns().addAll(filename,filetype,fileloc,lang);

		Button search = new Button();
		search.setText("Find Directory");
		GridPane.setConstraints(search,0,1);


		String dbURl = "jdbc:mysql://localhost/Movies";
        String user = "root";
        String pass = "amal";
        try{
        Connection connect = DriverManager.getConnection(dbURl,user,pass);
        statement = connect.createStatement();
        System.out.println("Connection established");
        
        
       

        
        

		EventHandler<ActionEvent> find_Dir = (ActionEvent e) ->{
			DirectoryChooser dirchooser = new DirectoryChooser();
			File dir = dirchooser.showDialog(null);

			if(dir !=null){
        		scanner.find_Fdata(dir,filedata);
        		insert(filedata, statement);
        		display(query, statement);
        		try{
        		connect.close();
        		table.setItems(data);
        		}catch(SQLException c){
        			System.out.println("Error Closing connection");
        			System.out.println(c.getMessage());

        		}	
			}

		};

		search.setOnAction(find_Dir);
		
		}catch(SQLException e){
        	System.out.println("No Database created");
        	System.out.println(e.getMessage());
        }

		window.getChildren().addAll(table, search);
		Scene scene = new Scene(window, 610,450);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Movie Database");
		primaryStage.show();

	}
	public void display(String query, Statement statement){
        try{
            ResultSet results = statement.executeQuery(query);
            //System.out.println("File name\tfileType\tfileLoc\t\tlanguage");
            while(results.next()){
                String fileName = results.getString("fileName");
                String fileType = results.getString("fileType");
                String fileLoc = results.getString("fileLoc");
                String language = results.getString("language");
                //System.out.println(fileName+"\t \t"+fileType+"\t \t"+fileLoc+"\t"+language);
                data.add(new table_data(fileName,fileType,fileLoc,language));
                //data.add(new table_data("Movie1", "Mkv","C://","Malayalam"));
            }
        }catch(SQLException ex){
            System.out.println("SQL Error:"+ex.getMessage());
        }
    }

	public static void insert(ArrayList<file> A, Statement statement){
        try{
            for(int i =0; i<A.size(); i++){
                String fileName = A.get(i).getFile_Name();
                String fileType = A.get(i).getFile_Type();
                String fileLoc = A.get(i).getFile_Loc();
                String language = A.get(i).getLanguage();
                String insert = "INSERT IGNORE INTO Files VALUES("+"\'"+fileName+"\'"+","+"\'"+fileType+"\'"+","+"\'"+fileLoc+"\'"+","+"\'"+language+"\'"+")";
                //System.out.println(insert);
                statement.executeUpdate(insert);
            }
            
        }catch(SQLException e){
            System.out.println("Error adding to Database");
            System.out.println(e.getMessage());
        }
        

    }
	public static void main(String [] args){
		launch(args);
	}
}