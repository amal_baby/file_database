import java.io.File;
import java.util.ArrayList;

public class find_Data {

    public find_Data() {

    }

    public void find_Fdata(File dir,ArrayList<file> A) {
        String[] supported_files = new String[]{"mp4", "avi", "mpeg", "wmv", "mkv"};
        String[] languages = new String[]{"Malayalam", "English", "Tamil", "Hindi"};
        if (dir.isDirectory() && !dir.isHidden()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    for (String type : supported_files) {
                        if (file.getName().contains(type)) {
                            String extention = file.getName().substring(file.getName().lastIndexOf("."));
                                for (String languge : languages) {
                                    if (file.getPath().contains(languge)) {
                                        file f = new file(file.getName(), extention, file.getPath(), languge);
                                        A.add(f);
                                    }
                                }
                        }
                    }

                } else {
                    find_Fdata(file, A);
                }
            }
        }
        
    }

}
