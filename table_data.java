public class table_data{
	private String name;
	private String type;
	private String loc;
	private String lang;

	public table_data(String name, String type, String loc, String lang){
		this.name = name;
		this.type = type;
		this.loc = loc;
		this.lang = lang;

	}
	public String getName(){
		return name;
	}
	public String getType(){
		return type;
	}
	public String getLoc(){
		return loc;
	}
	public String getLang(){
		return lang;
	}
}