public class file {
    private String file_Name;
    private String file_Type;
    private String file_Loc;
    private String language;

    public String getFile_Name() {
        return file_Name;
    }

    public String getFile_Type() {
        return file_Type;
    }

    public String getFile_Loc() {
        return file_Loc;
    }

    public file(String file_Name, String file_Type, String file_Loc, String Language) {
        this.file_Name = file_Name;
        this.file_Type = file_Type;
        this.file_Loc = file_Loc;
        this.language = Language;
    }

    public String getLanguage() {
        return language;
    }
}
